//Pierre DA SILVA
//Je pr�f�re pr�ciser que l'on m'a aid�
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "main.h"

float perimetrep(points *pt)
{
    float resultat = 0;

    for(int i=0; i < 6; i++)
    {
        resultat = resultat + sqrt(pow(pt[i+1].x-pt[i].x,2)+pow(pt[i+1].y-pt[i].y,2));
    }
    return resultat;
}

int main()
{
    points polygone[]=
    {
    {2,0},
    {6,0},
    {10,2},
    {8,6},
    {2,8},
    {0,4},
    {2,0}
    };

    float resultat=perimetrep(polygone);
    printf("%.2f \n", resultat);

    return 0;
}
